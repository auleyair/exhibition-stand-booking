<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/auleyair
 * @since      1.0.0
 *
 * @package    Exhibition_stand_booking
 * @subpackage Exhibition_stand_booking/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
