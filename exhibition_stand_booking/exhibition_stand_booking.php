<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/auleyair
 * @since             1.0.0
 * @package           Exhibition_stand_booking
 *
 * @wordpress-plugin
 * Plugin Name:       Exhibition Stand Booking
 * Plugin URI:        https://bitbucket.org/auleyair/exhibition-stand-booking
 * Description:       Exhibition Sales and Management Tool which enable you to create your own floor plan for particular event then publish it to Public. Public have a facility to select your own stand and book it. You have a power to approve the booking. 
 * Version:           1.0.0
 * Author:            Pauraic Mc Auley
 * Author URI:        https://bitbucket.org/auleyair
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       exhibition_stand_booking
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

$stand_space_array = array(
		array(type =>"C11", text => "1M X 1M"),
		array(type =>"C21", text => "2M X 1M"),
		array(type =>"C12", text => "1M X 2M"),
		array(type =>"C22", text => "2M X 2M"),
		array(type =>"C32", text => "3M X 2M"),
);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-exhibition_stand_booking-activator.php
 */


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-stand-map-template.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class-page-template.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/admin-page.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */

class exhibition_stand_booking{
	
	function __construct(){
		
		add_action('init', array($this, 'init'), 1);
		add_action('add_meta_boxes_stand_booking_map', array($this, 'stand_map_meta_box'), 1);
		add_action('add_meta_boxes_stand_booking', array($this, 'stand_booking_meta_box'), 1);
		add_action('add_meta_boxes_events', array($this, 'events_meta_box'), 1);
		add_action( 'wp_enqueue_scripts', array($this, 'stand_map_add_script'), 1);
		add_action( 'wp_enqueue_scripts', array($this, 'stand_map_add_style'), 1);
		add_action('save_post', array($this, 'stand_map_save_post_callback'), 1);
		add_action('save_post', array($this, 'stand_booking_save_post_callback'), 1);
		add_action('save_post', array($this, 'events_save_post_callback'), 1);
		add_filter('single_template', 'events_template');

		
		add_action( 'admin_enqueue_scripts', array($this, 'stand_map_add_script'), 1);
		add_action( 'admin_enqueue_scripts', array($this, 'stand_map_add_style'), 1);
		add_action('wp_ajax_nopriv_make_stand_booking', 'make_stand_booking');
		add_action('wp_ajax_make_stand_booking', 'make_stand_booking');
		add_action('wp_ajax_check_stand_availablity', 'check_stand_availablity');
		
		add_filter( 'manage_stand_booking_posts_columns', 'set_custom_edit_stand_booking_columns' );
		add_action( 'manage_stand_booking_posts_custom_column' , 'custom_stand_booking_column', 10, 2 );
		
	}
	function init(){
		
		// Create ACF post type
		$labels = array(
				'name' => __( 'Floor Plans', 'stand_booking_map' ),
				'singular_name' => __( 'Floor Plan', 'stand_booking_map' ),
				'add_new' => __( 'Add New Floor Plan' , 'stand_booking_map' ),
				'add_new_item' =>__( 'Add New Floor Plan' , 'stand_booking_map' )
		);
		
		register_post_type('stand_booking_map', array(
				'labels' => $labels,
				'public' => false,
				'show_ui' => true,
				'_builtin' =>  false,
				'capability_type' => 'page',
				'hierarchical' => true,
				'rewrite' => true,
				'query_var' => "stand_booking_map",
				'supports' => array(
						'title',
				),
				'show_in_menu'	=> true,
		));
		
		$labels = array(
				'name' => __( 'Stand Bookings', 'stand_booking' ),
				'singular_name' => __( 'Stand Booking', 'stand_booking' ),
				'add_new' => __( 'Add New Booking' , 'stand_booking' ),
				'add_new_item' =>__( 'Add New Booking' , 'stand_booking' )
		);
		
		register_post_type('stand_booking', array(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true,
				'_builtin' =>  false,
				'capability_type' => 'page',
				'hierarchical' => true,
				'rewrite' => true,
				'query_var' => "stand_booking",
				'supports' => array(
						'title',
				),
				'show_in_menu'	=> true,
		));
		
		$labels = array(
				'name' => __( 'Events', 'events' ),
				'singular_name' => __( 'Event', 'events' ),
				'add_new' => __( 'Add New Event' , 'events' ),
				'add_new_item' =>__( 'Add New Event' , 'events' )
		);
		
		register_post_type('events', array(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true,
				'_builtin' =>  false,
				'capability_type' => 'page',
				'hierarchical' => true,
				'rewrite' => true,
				'query_var' => "events",
				'supports' => array(
						'title',
				),
				'show_in_menu'	=> true,
		));
	}
	function stand_map_meta_box() {
		
		add_meta_box(
				'stand_map',
				__( 'Floor Plan', 'stand_booking_map' ),
				'stand_map_meta_box_callback'
				);
	}
	function stand_booking_meta_box(){
		add_meta_box(
				'stand_booking_event_id',
				__( 'Event ID', 'stand_booking' ),
				'stand_booking_event_id_meta_box_callback'
				);
		add_meta_box(
				'stand_booking_company',
				__( 'Company', 'stand_booking' ),
				'stand_booking_company_meta_box_callback'
				);
		add_meta_box(
				'stand_booking_name',
				__( 'Full Name', 'stand_booking' ),
				'stand_booking_name_meta_box_callback'
				);
		add_meta_box(
				'stand_booking_phone_number',
				__( 'Phone Number', 'stand_booking' ),
				'stand_booking_phone_number_meta_box_callback'
				);
		add_meta_box(
				'stand_booking_email',
				__( 'Email', 'stand_booking' ),
				'stand_booking_email_meta_box_callback'
				);
		add_meta_box(
				'stand_booking_stand_no',
				__( 'Stand No', 'stand_booking' ),
				'stand_booking_stand_no_meta_box_callback'
				);
		add_meta_box(
				'stand_booking_stand_size',
				__( 'Stand Size', 'stand_booking' ),
				'stand_booking_stand_size_meta_box_callback'
				);
		add_meta_box(
				'stand_booking_status',
				__( 'Status', 'stand_booking' ),
				'stand_booking_status_meta_box_callback'
				);
	}
	function events_meta_box(){
		add_meta_box(
				'event_date',
				__( 'Date of Event', 'events' ),
				'event_date_callback'
				);
		add_meta_box(
				'event_stand_map_id',
				__( 'Floor Map', 'events' ),
				'event_stand_map_callback'
				);
		
	}
	function stand_map_add_script()
	{
		// Register the script like this for a plugin:
		wp_register_script( 'custom-script', plugins_url( '/public/js/classie.js', __FILE__ ), array( 'jquery' ), NULL, false );
		wp_register_script( 'custom-script-1', plugins_url( 'public/js/custom.js', __FILE__ ), array( 'jquery' ), NULL, false );
		wp_register_script( 'custom-script-2', plugins_url( 'public/js/custom-front-end.js', __FILE__ ), array( 'jquery' ), NULL, false );
		wp_register_script( 'custom-script-3', plugins_url( 'public/js/popper.min.js', __FILE__ ), array( 'jquery' ), NULL, false );
		wp_register_script( 'custom-script-4', plugins_url( 'public/js/bootstrap.min.js', __FILE__ ), array( 'jquery' ), NULL, false );
		wp_localize_script( 'custom-script-2', 'MBAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'ajax_nonce' => wp_create_nonce('make_stand_booking_nonce') ) );
		// For either a plugin or a theme, you can then enqueue the script:
		wp_enqueue_script( 'custom-script' );
		wp_enqueue_script( 'custom-script-1' );
		wp_enqueue_script( 'custom-script-2' );
		wp_enqueue_script( 'custom-script-3' );
		wp_enqueue_script( 'custom-script-4' );
	}
	
	function stand_map_add_style()
	{
		// Register the style like this for a plugin:
		wp_register_style( 'custom-style', plugins_url( '/public/css/normalize.css', __FILE__ ), array(), '20180213', 'all' );
		wp_register_style( 'custom-style-1', plugins_url( '/public/fonts/font-awesome-4.2.0/css/font-awesome.min.css', __FILE__ ), array(), '201280213', 'all' );
		wp_register_style( 'custom-style-2', plugins_url( '/public/css/set1.css', __FILE__ ), array(), '20180213', 'all' );
		wp_register_style( 'custom-style-3', plugins_url( '/public/css/custom.css', __FILE__ ), array(), '20180213', 'all' );
		
		// For either a plugin or a theme, you can then enqueue the style:
		wp_enqueue_style( 'custom-style' );
		wp_enqueue_style( 'custom-style-1' );
		wp_enqueue_style( 'custom-style-2' );
		wp_enqueue_style( 'custom-style-3' );
	}
	
	
	function stand_map_save_post_callback($post_id){
		global $post;
		if ($post->post_type != 'stand_booking_map'){
			return;
		}
		
		// Check if our nonce is set.
		if ( ! isset( $_POST['stand_map_nonce'] ) ) {
			return;
		}
		
		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['stand_map_nonce'], 'stand_map_nonce' ) ) {
			return;
		}
		
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		
		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
			
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}
			
		}
		else {
			
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		}
		
		/* OK, it's safe for us to save the data now. */
		
		// Make sure that it is set.
		if ( ! isset( $_POST['stand_map_json'] ) ) {
			return;
		}
		
		// Sanitize user input.
		$my_data = sanitize_text_field( $_POST['stand_map_json'] );
		
		// Update the meta field in the database.
		update_post_meta( $post_id, '_stand_map', $my_data );
		
		
		
	}
	
	function stand_booking_save_post_callback($post_id){
		global $post;
		if ($post->post_type != 'stand_booking'){
			return;
		}
	
		// Check if our nonce is set.
		if ( ! isset( $_POST['stand_booking_nonce'] ) ) {
			return;
		}
	
		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['stand_booking_nonce'], 'stand_booking_nonce' ) ) {
			return;
		}
	
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
	
		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
				
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}
				
		}
		else {
				
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		}
	    error_log("Got this far on stand booking");
		/* OK, it's safe for us to save the data now. */
	
		// Make sure that it is set.
		if ( ! isset( $_POST['stand_booking_event_id']) ||
			 ! isset( $_POST['stand_booking_company']) ||
			 ! isset( $_POST['stand_booking_name']) ||
			 ! isset( $_POST['stand_booking_phone_number']) ||
			 ! isset( $_POST['stand_booking_email']) ||
			 ! isset( $_POST['stand_booking_stand_no']) ||
			 ! isset( $_POST['stand_booking_stand_size']) ||
			 ! isset( $_POST['stand_booking_status'])) {
			return;
		}
	
		// Sanitize user input.
		$event_id = sanitize_text_field( $_POST['stand_booking_event_id'] );
		$company = sanitize_text_field( $_POST['stand_booking_company'] );
		$name = sanitize_text_field( $_POST['stand_booking_name'] );
		$phoneNumber = sanitize_text_field( $_POST['stand_booking_phone_number'] );
		$email = sanitize_text_field( $_POST['stand_booking_email'] );
		$stand_no = sanitize_text_field( $_POST['stand_booking_stand_no'] );
		$status = sanitize_text_field( $_POST['stand_booking_status'] );
		$stand_size = sanitize_text_field( $_POST['stand_booking_stand_size'] );
		// Update the meta field in the database.
		
		update_post_meta($post_id, '_stand_booking_event_id', $event_id);
		update_post_meta($post_id, '_stand_booking_company', $company);
		update_post_meta($post_id, '_stand_booking_name', $name);
		update_post_meta($post_id, '_stand_booking_email', $email);
		update_post_meta($post_id, '_stand_booking_phone', $phoneNumber);
		update_post_meta($post_id, '_stand_booking_stand_no', $stand_no);
		update_post_meta($post_id, '_stand_booking_stand_size', $stand_size);
		update_post_meta($post_id, '_stand_booking_status', $status);
	
	
	
	}
	
	
	function events_save_post_callback($post_id){
		global $post;
		if ($post->post_type != 'events'){
			return;
		}
		
		// Check if our nonce is set.
		if ( ! isset( $_POST['events_nonce'] ) ) {
			return;
		}
		
		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['events_nonce'], 'events_nonce' ) ) {
			return;
		}
		
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		
		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
			
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}
			
		}
		else {
			
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		}
		
		/* OK, it's safe for us to save the data now. */
		
		// Make sure that it is set.
		if ( ! isset( $_POST['event_stand_map_id'] ) || ! isset( $_POST['event_date'] ) ) {
			return;
		}
		
		// Sanitize user input.
		$mapId = sanitize_text_field( $_POST['event_stand_map_id'] );
		$date = sanitize_text_field( $_POST['event_date'] );
		
		// Update the meta field in the database.
		update_post_meta( $post_id, '_event_stand_map_id', $mapId );
		update_post_meta( $post_id, '_event_date', $date );
		
		
		
	}

}
function make_stand_booking(){
	
	
	$data = $_POST["data"];
	
	$arr = array();
	$err = guestFormValidated($data);
	if(sizeof($err) < 1){
	
	
		$post_id = wp_insert_post(array (
				'post_type' => 'stand_booking',
				'post_title' => getValueFromData($data, "company"),
				'post_content' => "",
				'post_status' => 'publish',
				'comment_status' => 'closed',   // if you prefer
				'ping_status' => 'closed',      // if you prefer
		));
		
		if ($post_id) {
			// insert post meta
			add_post_meta($post_id, '_stand_booking_event_id', getValueFromData($data, "event_id"));
			add_post_meta($post_id, '_stand_booking_company', getValueFromData($data, "company"));
			add_post_meta($post_id, '_stand_booking_name', getValueFromData($data, "name"));
			add_post_meta($post_id, '_stand_booking_email', getValueFromData($data, "email"));
			add_post_meta($post_id, '_stand_booking_phone', getValueFromData($data, "phone"));
			add_post_meta($post_id, '_stand_booking_stand_no', getValueFromData($data, "stand_no"));
			add_post_meta($post_id, '_stand_booking_stand_size', getValueFromData($data, "stand_size"));
			add_post_meta($post_id, '_stand_booking_status', "PENDING");
		}
		
		
		$arr["status"] = "SUCCESS";
		$arr["message"] = "Booking Submission Successfully, you will be contacted shortly";
		
	
	}else{
		
		
		$arr["status"] = "FAILED";
		$arr["error"] = $err;
		
		
	}
	
	echo json_encode($arr);
	exit;
	
}

function guestFormValidated($data){
	
	$error = array();
	
	
	if ( ! wp_verify_nonce( $_POST['make_stand_booking_nonce'], 'make_stand_booking_nonce')) {
		
		$error[] = "Permission denied";
		
		return $error;
	}
	
	
	//event_id
	$eventId = getValueFromData($data, "event_id");
	if($eventId == NULL || $eventId == "" || intval($eventId) < 1){
		$error[] = "Event Id is not correct";
	}
	
	//company
	
	$company = getValueFromData($data, "company");
	if($company == NULL || $company == ""){
		$error[] = "Company is not valid";
	}
	
	//name
	
	$name = getValueFromData($data, "name");
	if($name == NULL || $name == ""){
		$error[] = "Name is not valid";
	}
	
	//email
	
	$email = getValueFromData($data, "email");
	if($email == NULL || $email == ""){
		$error[] = "Email is not valid";
	}else{
		
		$email = sanitize_email($email);
		
		if (!is_email($email)) {
			$error[] = "Email not format correctly";
		}
	}
	//phone
	
	$phone = getValueFromData($data, "phone");
	if($phone == NULL || $phone == "" || intval($phone) < 10000){
		$error[] = "Phone Number is not correct";
	}
	
	//stand no 
	
	$stand_no = getValueFromData($data, "stand_no");
	if($stand_no == NULL || $stand_no == ""){
		$error[] = "Stand No is not correct";
	}
	
	//TODO: Check if the stand no is on the map.
	$stand_map_id = get_post_meta($eventId, "_event_stand_map_id", true);
	$stand_map_json = get_post_meta($stand_map_id, "_stand_map", true);
	
	$stand_map = json_decode($stand_map_json, true);
	
	if(!checkIfStandExisted($stand_map, $stand_no)){
		$error[] = "Stand No does not exist.";
	}
	
	return $error;
}


function check_stand_availablity(){
	
	$status = "AVAILABLE";
	$message = "";
	$error = false;
	
	$eventId = $_POST["event_id"];
	$stand_no = $_POST["stand_no"];
	$bookingId = $_POST["booking_id"];
	
	if($eventId == "" || $eventId == null){
		$status = "ERROR";
		$message = "Event Id is Required";
		$error = true;
	}
	
	if($stand_no == "" || $stand_no == null){
		$status = "ERROR";
		$message = "Stand No is Required";
		$error = true;
	}
	
	if($bookingId == "" || $bookingId == null){
		$status = "ERROR";
		$message = "Stand No is Required";
		$error = true;
	}
	
	if($error == false){
		
	
		$loop = new WP_Query( array(
				'post_type' => 'stand_booking',
				'posts_per_page' => -1,
				'meta_query' => array(
						array('key' => '_stand_booking_event_id', 'value' => $eventId),
						array('key' => '_stand_booking_stand_no', 'value' => $stand_no)
				)
		) );
		
	
		
		if ( $loop->have_posts() ) {
		
			while ( $loop->have_posts() ) {
				$loop->the_post();
				$status = get_post_meta( get_the_ID() , '_stand_booking_status' , true );
				
				switch ($status){
					case "PENDING":
						$company = get_post_meta( get_the_ID() , '_stand_booking_company' , true );
						$message = "$company have made a booking on this stand and it marked as Pending.";
						break;
					case "CONFIRMED":
						$company = get_post_meta( get_the_ID() , '_stand_booking_company' , true );
						$message = "$company have made a confirmed booking on this stand therefore not available";
						break;
				}
				
				
				if($bookingId == get_the_ID() && $stand_no == get_post_meta( get_the_ID() , '_stand_booking_stand_no' , true )){
					
					$status = "NO_CHANGE";
					$message = "";
					
				}
				
				
					
			}
		
			/* Restore original Post Data */
			wp_reset_postdata();
		}
	
	}
	
	$data = array();
	$data["status"] =  $status;
	$data["message"] = $message;
	
	echo json_encode($data);
	exit;
	
}

function getValueFromData($data, $key){
	
	foreach ($data as $d){
		
		if(strcmp($d["name"], $key) == 0){
			return sanitize_text_field($d["value"]);
		}
		
		
	}
	return null;
	
}

function  checkIfStandExisted($data, $id){
	
	foreach($data['stands'] as $d){
		
		if(strcmp($id, $d["standId"]) == 0){
			return true;
		}
		
	}
	
	return false;
}

function run_exhibition_stand_booking() {

	global $sbm;
	
	if( !isset($sbm) )
	{
		$sbm = new exhibition_stand_booking();
	}
	
	return $sbm;

}
run_exhibition_stand_booking();
