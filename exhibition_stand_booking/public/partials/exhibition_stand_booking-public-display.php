<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/auleyair
 * @since      1.0.0
 *
 * @package    Exhibition_stand_booking
 * @subpackage Exhibition_stand_booking/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
