var bookedStand = [];
jQuery("#frontend_stand_map").ready(function($){
	adminMode = 0;
	var preexisting1 = $("#frontend_stand_map_json").val();
	
	
	if(IsJsonString(preexisting1)){
		
		var json1 = JSON.parse(preexisting1);
		//gridRatio = 0.94;
		
		if(json1.width >= 70){
			//gridRatio = 0.91;
		}else if(json1.width >= 50 && json1.width < 70){
			//gridRatio = 0.97;
		}else if(json1.width >= 20 && json1.width < 50){
			//gridRatio = 0.98;
		}
		
		createGrid(json1.height, json1.width, "#frontend_stand_map");
		liveView = true;
		$.each(json1.stands, drawIntoGridPublic);
		$.each(json1.structure, drawIntoGridPublic);
		
		
		
		
		
		$("#gridbox").removeClass("grid").addClass("grid-output");
		$(".walkway").addClass("walkway-output");
		
	}else{
		
		$("#frontend_stand_map").html("<h2>Exhibition Stand Map is not available now</h2>");
		
	}
	
	//console.log(MBAjax);
	
	jQuery('#ajax-booking-form').submit(ajaxSubmit);
	
	if(bookedStand !== undefined){
		if(bookedStand.length > 0){
			setupMap(bookedStand);
		}
	}

	
	$('[data-toggle="tooltip"]').tooltip(); 

});

var selected = "";

function userSelect(elm){
	
	var id = $(elm).attr("id");
	
	if($("#"+id).hasClass("standBooked")){
		return;
	}
	
	if(selected != ""){
	
		
		removeUserSelected(selected);
	}
	
	
	selected = id;
	addUserSelected(selected);
	
}

function removeUserSelected(id){
	
	
	var len = exbSpace.length; 
	
	for(var i = 0; i < len; i++){
		const index = exbSpace[i].indexOf(id);
		
		if(index !== -1){
			var slot = exbSpace[i];
			for(var j = 0; j < slot.length; j++){
				
				$("#" + slot[j]).removeClass("userselected");
			}
			return;
		}
	}
	
	
}

function addUserSelected(id){
	
	
	var len = exbSpace.length; 
	
	for(var i = 0; i < len; i++){
		const index = exbSpace[i].indexOf(id);
		
		if(index !== -1){
			var slot = exbSpace[i];
			for(var j = 0; j < slot.length; j++){
				
				$("#" + slot[j]).addClass("userselected");
				if(checkIfFirstSelectedElement(slot[j])){
					$("#stand_no").val($("#" + slot[j]).attr("data-stand-id"));
					$("#stand_size").val($("#" + slot[j]).attr("data-stand-type"));
				}
			}
			return;
		}
	}
	

	
}

function ajaxSubmit() {
	
	jQuery("#feedback").empty();
    var BookingForm = jQuery(this).serializeArray();
   
    jQuery.ajax({      
      type:    "POST",
      url:     MBAjax.ajaxurl,
      dataType: "json",
      data:    {
    	  "action":'make_stand_booking',
    	  "make_stand_booking_nonce": MBAjax.ajax_nonce,
    	  "data":BookingForm
      },
      success: function(data) {
    	  
    	  if(data.status == "SUCCESS"){
    		  jQuery("#feedback").html(data.message); 
    		  $("#ajax-booking-form").hide();
    	  }else{
    		  var ul = $("<ul/>").appendTo("#feedback");
    		  $.each(data.error, function(i, err){
    			 
    			  $("<ol/>").appendTo(ul).html(err);
    			  
    		  });
    	  }
         
        
      }
    });
    return false;
  }


function setupMap(data){
	
	$.each(data, function(i , sid){
		
		var id = $("[data-stand-id="+sid.standId+"]").attr("id"); 
		var len = exbSpace.length; 
		
		for(var i = 0; i < len; i++){
			const index = exbSpace[i].indexOf(id);
			
			if(index !== -1){
				var slot = exbSpace[i];
				for(var j = 0; j < slot.length; j++){
					
					$("#" + slot[j]).removeClass("selectedPublic").addClass("standBooked");
					
					$("#"+ slot[j]).attr({"data-toggle":"tooltip", "title":sid.title});
				}
				return;
			}
		}
		
		
	});
	
}


function drawIntoGridPublic(i, elm){
	
	var loc = "#" + elm.standLocation;
	
	if(editMode || liveView){
		
		switch(elm.standType){
			case "C11": draw1x1Public(loc); break;
			case "C12": draw1x2Public(loc); break;
			case "C21": draw2x1Public(loc); break;
			case "C22": draw2x2Public(loc); break;
			case "C23": draw2x3Public(loc); break;
			case "C32": draw3x2Public(loc); break;
			case "C33": draw3x3Public(loc); break;
			case "C66": draw6x6Public(loc); break;
			case "walkway": drawWalkwayPublic(loc); break;
			case "wallV": drawWall(loc, "V"); break;
			case "wallH": drawWall(loc, "H"); break;
			case "wallD"	: drawWall(loc, "D"); break;
			case "wallB": drawWall(loc, "B"); break;
			case "wallT": drawWall(loc, "T"); break;
			case "wallTC" : drawWall(loc, "TC"); break;
			case "wallEP": drawWall(loc, "EP"); break;
			case "wallE3P" : drawWall(loc, "E3P"); break;
			case "wallPH" : drawWall(loc, "PH"); break;
			case "wallWH": drawWall(loc, "WH"); break;
			case "wallWV": drawWall(loc, "WV"); break;
		}
	}
	return;
	
	
}

function draw1x1Public(elm){
	var id = $(elm).attr("id");
	if($(elm).hasClass("selected")){
		$(elm).removeClass("selected");
		removeFromExb(id);
		
	}else{
		$(elm).addClass("selectedPublic corner-all").attr("data-stand-type", "C11");
		exbSpace.push([id]);
		
		spaceCount.c11++;
		$("#c11-value").html(spaceCount.c11);
		
	}
	generateStandNo();
	saveToFile();
	
}

function drawWalkwayPublic(elm){
	var id = $(elm).attr("id");
	var arrId = [id];
	if($(elm).hasClass("walkway")){
		$(elm).removeClass("walkway");
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("walkwayPublic").attr("data-stand-type", "walkway").attr("data-structure", true);
			exbSpace.push([id]);
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}
function draw1x2Public(elm){
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var arrId = [id, nextId];
	
	
	
		
		if($(elm).hasClass("selected")){
			
			removeFromExb(id);
			
		}else{
			if(checkIfNoConflict(arrId)){
				$(elm).addClass("selectedPublic top-left bottom-left").attr("data-stand-type", "C12");
				$("#" + nextId).addClass("selectedPublic top-right bottom-right");
				exbSpace.push(arrId);
				
				spaceCount.c21++;
				$("#c21-value").html(spaceCount.c21);
				
			}else{
				showHightlight(elm);
			}
		}
		generateStandNo();
		saveToFile();

}
function draw2x1Public(elm){
	var id = $(elm).attr("id");
	var nextId = getBlockOnBelow(id);
	var arrId = [id, nextId];
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
			
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selectedPublic top-left top-right").attr("data-stand-type", "C21");
			$("#" + nextId).addClass("selectedPublic bottom-left bottom-right");
			exbSpace.push([id, nextId]);
			spaceCount.c21++;
			$("#c21-value").html(spaceCount.c21);
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}
function draw2x2Public(elm){
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	
	var arrId = [id, nextId, belowId, nextBelowId];
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selectedPublic C22-bottom C22-right top-left").attr("data-stand-type", "C22");
			$("#" + nextId).addClass("selectedPublic C22-bottom top-right");
			$("#" + belowId).addClass("selectedPublic C22-right bottom-left");
			$("#" + nextBelowId).addClass("selectedPublic bottom-right");
			exbSpace.push(arrId);
			
			spaceCount.c22++;
			$("#c22-value").html(spaceCount.c22);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}


function draw2x3Public(elm){
	
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	var belowBelowId = getBlockOnBelow(belowId);
	var nextBelowBelowId = getBlockOnBelow(nextBelowId);
	
	var arrId = [id, nextId, belowId, nextBelowId, belowBelowId,  nextBelowBelowId];
	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selectedPublic top-left").attr("data-stand-type", "C23");
			$("#" + nextId).addClass("selectedPublic top-right");
			
			$("#" + belowId).addClass("selectedPublic");
			$("#" + nextBelowId).addClass("selectedPublic");
			
			$("#" + belowBelowId).addClass("selectedPublic bottom-left");
			
			$("#" + nextBelowBelowId).addClass("selectedPublic bottom-right");
			exbSpace.push(arrId);
			
			spaceCount.c23++;
			$("#c23-value").html(spaceCount.c23);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}

function draw3x2Public(elm){
	
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var nextId1 = getBlockOnRight(nextId);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	var nextBelowId1 = getBlockOnBelow(nextId1);
	
	var arrId = [id, nextId, nextId1, belowId, nextBelowId, nextBelowId1];
	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selectedPublic top-left").attr("data-stand-type", "C32");
			$("#" + nextId).addClass("selectedPublic ");
			$("#" + nextId1).addClass("selectedPublic  top-right");
			$("#" + belowId).addClass("selectedPublic  bottom-left");
			$("#" + nextBelowId).addClass("selectedPublic");
			$("#" + nextBelowId1).addClass("selectedPublic bottom-right");
			exbSpace.push(arrId);
			
			spaceCount.c23++;
			$("#c23-value").html(spaceCount.c23);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}
function draw3x3Public(elm){
	
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var nextId1 = getBlockOnRight(nextId);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	var nextBelowId1 = getBlockOnBelow(nextId1);
	var belowBelowId = getBlockOnBelow(belowId);
	var nextBelowBelowId = getBlockOnBelow(nextBelowId);
	var nextBelowBelowId1 = getBlockOnBelow(nextBelowId1);
	
	var arrId = [id, nextId, nextId1, belowId, nextBelowId, nextBelowId1, belowBelowId, nextBelowBelowId, nextBelowBelowId1];
	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selectedPublic top-left").attr("data-stand-type", "C33");
			$("#" + nextId).addClass("selectedPublic");
			$("#" + nextId1).addClass("selectedPublic top-right");
			$("#" + belowId).addClass("selectedPublic");
			$("#" + nextBelowId).addClass("selectedPublic");
			$("#" + nextBelowId1).addClass("selectedPublic");
			$("#" + belowBelowId).addClass("selectedPublic bottom-left");
			$("#" + nextBelowBelowId).addClass("selectedPublic");
			$("#" + nextBelowBelowId1).addClass("selectedPublic bottom-right");
			
			exbSpace.push(arrId);
			
			spaceCount.c33++;
			$("#c33-value").html(spaceCount.c33);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}

function draw6x6Public(elm){
	
	var box = {};
	
	var arrId = [];
    for(var r = 1; r <= 6; r++){
    	for(var c = 1; c <= 6;c++){
    		if(r == 1 && c == 1){
    			box["r1c1"] =$(elm).attr("id");
    		}else if(c == 1){
    			box["r"+r+"c1"] =getBlockOnBelow(box["r"+(r-1)+"c1"]);
    		}else{
    			box["r"+r+"c"+c] =getBlockOnRight(box["r"+r+"c"+(c-1)]);
    		}
    		
    		arrId.push(box["r"+r+"c"+c]);
    	}
    	
    }

	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(box["r1c1"]);
		
	}else{
		if(checkIfNoConflict(arrId)){
			
			
		    for(var r = 1; r <= 6; r++){
		    	for(var c = 1; c <= 6;c++){
		    		
		    		$("#" + box["r"+r+"c"+c]).addClass("selectedPublic");
		    		$("#" + box["r1c1"]).addClass("top-left").attr("data-stand-type", "C66");
		    		$("#" + box["r1c6"]).addClass("top-right");
		    		$("#" + box["r6c1"]).addClass("bottom-left");
		    		$("#" + box["r6c6"]).addClass("bottom-right");
		    		
		    	}
		    }
			

			
			
			exbSpace.push(arrId);
			
			spaceCount.c66++;
			$("#c66-value").html(spaceCount.c66);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}
