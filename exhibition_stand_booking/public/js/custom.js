var spaceSelect = "s11";
var exbSpace = [];
var editMode = false;
var liveView = false;
var $ = jQuery;
var floorplanStage = 1;
var gridRatio = 1;
var adminMode = 1;

var rotation = 0;

var spaceCount = {
		
		c11:0,
		c12:0,
		c21:0,
		c22:0,
		c23:0,
		c32:0,
		c33:0,
		c66:0,
		total:0
		
}

jQuery("#stand_map_area").ready(function($){
	var stand_no_input = $("#stand_booking_stand_no_input");
	if(stand_no_input.length > 0){
		
		//$("#stand_booking_stand_no_input")
		stand_no_input.change(function(){
			
			checkStandIfAvailable(this);
			
		});
	}
	
	if($("#stand_map_area").length < 1){
		return;
	}
	
	var mousedown = false;
	$(document).mousedown(function() {
	    mousedown = true;
	}).mouseup(function() {
	    mousedown = false;  
	});
	
	var preexisting = $("#stand_map_json").val();
	
	if(IsJsonString(preexisting)){
		
		var json = JSON.parse(preexisting);
		
		
		
		editMode = true;
		createGrid(json.height, json.width, "#gridArea");
		
		$.each(json.stands, drawIntoGrid);
		$.each(json.structure, drawIntoGrid);
		
		
		floorplanSecondStageNext();
		floorplanStage = 3;
		
	}else{
		//floorplanfirstStage();
		$("#floorplan_back").hide();
		$("#second_stage").hide();
		$("#third_stage").hide();
		createGrid(10, 10, "#gridArea");
		$("#height_input").val(10);
		$("#width_input").val(10);

		
	}
	
	$("#floorplan_next").click(function(){
		switch(floorplanStage){
			case 1:
				floorplanfirstStageNext();
				break;
			case 2:
				floorplanSecondStageNext();
				break;
			case 3:
				floorplanThirdStageNext();
				break;
		}
	});
	
	$("#floorplan_back").click(function(){
		switch(floorplanStage){
			case 1:
				floorplanfirstStageBack();
				break;
			case 2:
				floorplanSecondStageBack();
				break;
			case 3:
				floorplanThirdStageBack();
				break;
			case 4: 
				floorplanFourthStageBack();
				break;
		}
		addRotatePreview(selectedType, rotation);
	});
	$("#btnRotateLeft").click(function(){
		if(editMode){
			switch (rotation) {
			case 0:
				rotation = 270;
				break;
			case 270:
				rotation = 180;
				break;
			case 180: 
				rotation = 90;
				break;
			case 90:
				rotation = 0;
				break;
			default:
				return;
			}
			addRotatePreview(selectedType, rotation);
		}
		
		
		
	});
	
	$("#btnRotateRight").click(function(){
		if(editMode){
			switch (rotation) {
			case 0:
				rotation = 90;
				break;
			case 90:
				rotation = 180;
				break;
			case 180: 
				rotation = 270;
				break;
			case 270:
				rotation = 0;
				break;
			default:
				return;
			}
			addRotatePreview(selectedType, rotation);
		}
		
	});
	
	

		
		$("#btns11").addClass("spaceSizeSelected");
		
		$("#btns11").click(function(){
			if(editMode){
				if(!$("#btns11").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s11";
					$("#btns11").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		$("#btns12").click(function(){
			if(editMode){
				if(!$("#btns12").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s12";
					$("#btns12").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		$("#btns21").click(function(){
			if(editMode){
				if(!$("#btns21").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s21";
					$("#btns21").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});	
		$("#btns22").click(function(){
			if(editMode){
				if(!$("#btns22").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s22";
					$("#btns22").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		$("#btns23").click(function(){
			if(editMode){
				if(!$("#btns23").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s23";
					$("#btns23").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		$("#btns32").click(function(){
			if(editMode){
				if(!$("#btns32").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s32";
					$("#btns32").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btns33").click(function(){
			if(editMode){
				if(!$("#btns33").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s33";
					$("#btns33").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btns66").click(function(){
			if(editMode){
				if(!$("#btns66").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "s66";
					$("#btns66").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnWalkway").click(function(){
			if(editMode){
				if(!$("#btnWalkway").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "Walkway";
					$("#btnWalkway").addClass("spaceSizeSelected");
					walkwaySelected = true;
				}
			}
		});
		
		$("#structure_area div").click(function(){
			
				var setRotation = $(this).attr("data-set-rotation");
				
				
				if(setRotation == null){
					$("#rotate").slideUp();
				}else{
					$("#rotate").slideDown();
				}
				
				
		})
		
		$("#btnWallH").click(function(){
			if(editMode){
				
				if(!$("#btnWallH").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "WallH";
					$("#btnWallH").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnWallV").click(function(){
			if(editMode){
				if(!$("#btnWallV").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "WallV";
					$("#btnWallV").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnWindowV").click(function(){
			if(editMode){
				if(!$("#btnWindowV").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "WindowV";
					$("#btnWindowV").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		
		$("#btnWindowH").click(function(){
			if(editMode){
				
				if(!$("#btnWindowH").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "WindowH";
					$("#btnWindowH").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});

		
		$("#btnStructureBlock").click(function(){
			if(editMode){
				if(!$("#btnStructureBlock").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "StructureBlock";
					$("#btnStructureBlock").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnPillarBlock").click(function(){
			if(editMode){
				if(!$("#btnPillarBlock").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "PillarBlock";
					$("#btnPillarBlock").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnDoor").click(function(){
			if(editMode){
				if(!$("#btnDoor").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "Door";
					$("#btnDoor").addClass("spaceSizeSelected");
					walkwaySelected = false;
					
					
					addRotatePreview("SD", 0);
					
				}
			}
		});
		
		$("#btnDoubleDoor").click(function(){
			if(editMode){
				if(!$("#btnDoubleDoor").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "DoubleDoor";
					$("#btnDoubleDoor").addClass("spaceSizeSelected");
					walkwaySelected = false;
					
					addRotatePreview("DD", 0);
				}
			}
		});
		
		$("#btnToliet").click(function(){
			if(editMode){
				if(!$("#btnToliet").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "Toliet";
					$("#btnToliet").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnTable").click(function(){
			if(editMode){
				if(!$("#btnTable").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "Table";
					$("#btnTable").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnPower").click(function(){
			if(editMode){
				if(!$("#btnPower").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "Power";
					$("#btnPower").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnThreePhase").click(function(){
			if(editMode){
				if(!$("#btnThreePhase").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "ThreePhase";
					$("#btnThreePhase").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});
		
		$("#btnPhone").click(function(){
			if(editMode){
				if(!$("#btnPhone").hasClass("spaceSizeSelected")){
					$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
					spaceSelect = "Phone";
					$("#btnPhone").addClass("spaceSizeSelected");
					walkwaySelected = false;
				}
			}
		});

		$("#generateStandNo").click(function(){
			if(editMode){
				generateStandNo();
			}
		});
		
		$("#showOutput").click(function(){
			

		});
		
		$("#returnToEdit").click(function(){
			
			$(".spaceSelection").show();
			$(".outputDisplay").hide();
			
			$("#gridbox").removeClass("grid-output").addClass("grid");
			$(".walkway").removeClass("walkway-output");
			editMode = true;
		});
		
		$("#saveToFile").click(function(){
			
			saveToFile();
			
		});
		
		$("#width_input").keyup(function(){
			$("#error").html("");
			var height = $("#height_input").val();
			var width = $("#width_input").val();
			
			if(height > 100 || width > 100 || (height > 100 && width > 100)){
				$("#error").html("Error: Height and Width must be greater than 10x10 and less than 100x100");
				return;
			}
			
			if(height < 10 || width < 10 || (height < 10 && width < 10)){
				$("#error").html("Error: Height and Width must be greater than 10x10 and less than 100x100");
				return;
			}
			adminMode = 1;
			createGrid(height, width, "#gridArea");
			fillPreExisting();
		})
		
		$("#height_input").keyup(function(){
			$("#error").html("");
			var height = $("#height_input").val();
			var width = $("#width_input").val();
			
			if(height > 100 || width > 100 || (height > 100 && width > 100)){
				$("#error").html("Error: Height and Width must be greater than 10x10 and less than 100x100");
				return;
			}
			
			if(height < 10 || width < 10 || (height < 10 && width < 10)){
				$("#error").html("Error: Height and Width must be greater than 10x10 and less than 100x100");
				return;
			}
			adminMode = 1;
			createGrid(height, width, "#gridArea");
			fillPreExisting();
		})
	

	spaceSelect = "s11";

});

var maxRow = 1;
var maxCol = 1;

var inputWidth;
var inputHeight;
var clazz = "gridBox";
function createGrid(ph, pw, element) {
	$("#gridArea").empty();
    var ratioW = inputWidth =  pw,
        ratioH = inputHeight = ph;
    
    var scale = 20;
    
    
    
    if(pw > 50){
    	scale = 8;
    	clazz = "gridBox-4";
    }else if(pw > 50){
    	scale = 10;
    	clazz = "gridBox-3";
    	
    }else if(pw > 25){
    	scale = 13;
    	clazz = "gridBox-2";
    	
    }else if(pw > 15){
    	scale = 15; 
    	clazz = "gridBox-1";
    }
    
    
    if(pw >= 70){
    	
    	$("#floorplanSummary").removeClass("move_left").addClass("move_right");
    }else{
    	$("#floorplanSummary").removeClass("move_right").addClass("move_left");
    }

    var parent = $('<div />', {
    	
    	id: "gridbox",
        class: 'grid',
        width: (ratioW  * (scale + adminMode)),//* gridRatio,
        height: (ratioH  * (scale + adminMode)), //* gridRatio
    }).addClass('grid').appendTo(element);

    var row = 1;//'a';
    var col =1;
    for (var i = 0; i < ratioH; i++) {
        for(var p = 0; p < ratioW; p++){
            $('<div />', {
            	id: "R" + row + "_C"+col,
            	class: clazz
            }).appendTo(parent).click(function(){
            	if(editMode){
            	drawSpace(this);
            	}else if(liveView){
            		userSelect(this);
            	}
            });
            maxCol = col;
            col++;
        }
        row = maxRow = (row+1);//getNextKey(row);
        col = 1;
    }
}

function getNextKey(key) {
	  if (key === 'Z' || key === 'z') {
	    return String.fromCharCode(key.charCodeAt() - 25) + String.fromCharCode(key.charCodeAt() - 25); // AA or aa
	  } else {
	    var lastChar = key.slice(-1);
	    var sub = key.slice(0, -1);
	    if (lastChar === 'Z' || lastChar === 'z') {
	      // If a string of length > 1 ends in Z/z,
	      // increment the string (excluding the last Z/z) recursively,
	      // and append A/a (depending on casing) to it
	      return getNextKey(sub) + String.fromCharCode(lastChar.charCodeAt() - 25);
	    } else {
	      // (take till last char) append with (increment last char)
	      return sub + String.fromCharCode(lastChar.charCodeAt() + 1);
	    }
	  }
	  return key;
	};


function drawSpace(elm){
	if(editMode){
	
		switch(spaceSelect){
			case "s11": draw1x1(elm); break;
			case "s12": draw1x2(elm); break;
			case "s21": draw2x1(elm); break;
			case "s22": draw2x2(elm); break;
			case "s23": draw2x3(elm); break;
			case "s32": draw3x2(elm); break;
			case "s33": draw3x3(elm); break;
			case "s66": draw6x6(elm); break;
			case "Walkway": drawWalkway(elm); break;
			case "WallV": drawWall(elm, "V"); break;
			case "WallH": drawWall(elm, "H"); break;
			case "Door"	: drawWall(elm, "D"); break;
			case "DoubleDoor"	: drawWall(elm, "DD"); break;
			case "Toliet": drawWall(elm, "T"); break;
			case "Table" : drawWall(elm, "TC"); break;
			case "PillarBlock" : drawWall(elm, "P"); break;
			case "StructureBlock": drawWall(elm, "B"); break;
			case "WindowV": drawWall(elm, "WV"); break;
			case "WindowH": drawWall(elm, "WH"); break;
			case "Power": drawWall(elm, "EP"); break;
			case "ThreePhase": drawWall(elm, "E3P"); break;
			case "Phone": drawWall(elm, "PH"); break;
		}
	}
	return;
	
}	
	
function draw1x1(elm){
	var id = $(elm).attr("id");
	if($(elm).hasClass("selected")){
		$(elm).removeClass("selected");
		removeFromExb(id);
		
	}else{
		$(elm).addClass("selected C11").attr("data-stand-type", "C11");
		exbSpace.push([id]);
		
		spaceCount.c11++;
		$("#c11-value").html(spaceCount.c11);
		spaceCount.total++;
		$("#standTotal").html(spaceCount.total);
		
		
	}
	generateStandNo();
	saveToFile();
	
}

function drawWalkway(elm){
	var id = $(elm).attr("id");
	var arrId = [id];
	if($(elm).hasClass("walkway")){
		$(elm).removeClass("walkway");
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("walkway").attr("data-stand-type", "walkway").attr("data-structure", true);
			exbSpace.push([id]);
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}
function drawWall(elm, type){
	var id = $(elm).attr("id");
	var arrId = [id];
	if($(elm).hasClass("wall")){
		$(elm).removeClass("wall");
		$(elm).empty();
		removeFromExb(id)
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("wall").css("line-height", 0.6);
			var path = $("#img_path").val();
			switch (type) {
			case 'H':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"wall-h.png' class='"+style+"' />");
				$(elm).attr("data-stand-type", "wallH").attr("data-structure", true);
				break;
			case 'V':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"wall-v.png' class='"+style+"' />").attr("data-stand-type", "wallV").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallV").attr("data-structure", true);
				break;
			case 'B':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"wall-block.png' class='"+style+"' />").attr("data-stand-type", "wallB").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallB").attr("data-structure", true);
				break;
				
			case 'T':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"toliet.jpg' class='"+style+"' />").attr("data-stand-type", "wallT").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallT").attr("data-structure", true);
				break;
			case 'TC':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"table.png' class='"+style+"' />").attr("data-stand-type", "wallTC").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallTC").attr("data-structure", true);
				break;
				
			case 'D':
				var style = $(elm).attr("class") + " " + rotationClass;
				$("<span/>").appendTo(elm).html("<img src='"+path+"single-door.png' class='"+style+"' />").attr("data-stand-type", "wallD").attr({"data-structure":true, "data-rotate":rotationClass});
				//$("<span/>").appendTo(elm).html("D").attr("data-stand-type", "wallD").attr("data-structure", true);
				$(elm).attr("data-stand-type", "wallD").attr("data-structure", true);
				break;
			case 'DD':
				var style = $(elm).attr("class") + " " + rotationClass;
				$("<span/>").appendTo(elm).html("<img src='"+path+"double-door.png' class='"+style+"' />").attr("data-stand-type", "wallD").attr({"data-structure":true, "data-rotate":rotationClass});
				//$("<span/>").appendTo(elm).html("D").attr("data-stand-type", "wallD").attr("data-structure", true);
				$(elm).attr("data-stand-type", "wallD").attr("data-structure", true);
				break;
			case 'P':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"pillar.png' class='"+style+"' />").attr("data-stand-type", "wallP").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallP").attr("data-structure", true);
				break;
			case 'WV':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"window-v.png' class='"+style+"' />").attr("data-stand-type", "wallWV").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallWV").attr("data-structure", true);
				break;
			case 'WH':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"window-h.png' class='"+style+"' />").attr("data-stand-type", "wallWH").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallWH").attr("data-structure", true);
				break;
			case 'EP':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"plug.jpg' class='"+style+"' />").attr("data-stand-type", "wallEP").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallEP").attr("data-structure", true);
				break;
			case 'E3P':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"threePhase.jpg' class='"+style+"' />").attr("data-stand-type", "wallE3P").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallE3P").attr("data-structure", true);
				break;
			case 'PH':
				var style = $(elm).attr("class");
				$("<span/>").appendTo(elm).html("<img src='"+path+"phone.png' class='"+style+"' />").attr("data-stand-type", "wallPH").attr("data-structure", true);;
				$(elm).attr("data-stand-type", "wallPH").attr("data-structure", true);
				break;
			default:
				break;
			}
			
			exbSpace.push([id]);
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
	
}
function draw1x2(elm){
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var arrId = [id, nextId];
	
	
	
		
		if($(elm).hasClass("selected")){
			
			removeFromExb(id);
			
		}else{
			if(checkIfNoConflict(arrId)){
				$(elm).addClass("selected C12 C12-right").attr("data-stand-type", "C12");
				$("#" + nextId).addClass("selected C12");
				exbSpace.push(arrId);
				
				spaceCount.c21++;
				$("#c21-value").html(spaceCount.c21);
				spaceCount.total++;
				$("#standTotal").html(spaceCount.total);
				
			}else{
				showHightlight(elm);
			}
		}
		generateStandNo();
		saveToFile();

}
function draw2x1(elm){
	var id = $(elm).attr("id");
	var nextId = getBlockOnBelow(id);
	var arrId = [id, nextId];
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
			
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selected C21 C21-bottom").attr("data-stand-type", "C21");
			$("#" + nextId).addClass("selected C21");
			exbSpace.push([id, nextId]);
			spaceCount.c21++;
			$("#c21-value").html(spaceCount.c21);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}
function draw2x2(elm){
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	
	var arrId = [id, nextId, belowId, nextBelowId];
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selected C22 C22-bottom C22-right").attr("data-stand-type", "C22");
			$("#" + nextId).addClass("selected C22 C22-bottom");
			$("#" + belowId).addClass("selected C22 C22-right");
			$("#" + nextBelowId).addClass("selected C22");
			exbSpace.push(arrId);
			
			spaceCount.c22++;
			$("#c22-value").html(spaceCount.c22);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}


function draw2x3(elm){
	
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	var belowBelowId = getBlockOnBelow(belowId);
	var nextBelowBelowId = getBlockOnBelow(nextBelowId);
	
	var arrId = [id, nextId, belowId, nextBelowId, belowBelowId,  nextBelowBelowId];
	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selected C23 C23-right C23-bottom").attr("data-stand-type", "C23");
			$("#" + nextId).addClass("selected C23 C23-bottom");
			
			$("#" + belowId).addClass("selected C23 C23-right C23-bottom");
			$("#" + nextBelowId).addClass("selected C23 C23-bottom ");
			
			$("#" + belowBelowId).addClass("selected C23 C23-right");
			
			$("#" + nextBelowBelowId).addClass("selected C23");
			exbSpace.push(arrId);
			
			spaceCount.c23++;
			$("#c23-value").html(spaceCount.c23);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}

function draw3x2(elm){
	
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var nextId1 = getBlockOnRight(nextId);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	var nextBelowId1 = getBlockOnBelow(nextId1);
	
	var arrId = [id, nextId, nextId1, belowId, nextBelowId, nextBelowId1];
	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selected C32 C32-right C32-bottom").attr("data-stand-type", "C32");
			$("#" + nextId).addClass("selected C32 C32-right C32-bottom");
			$("#" + nextId1).addClass("selected C32 C32-bottom");
			$("#" + belowId).addClass("selected C32 C32-right");
			$("#" + nextBelowId).addClass("selected C32 C32-right");
			$("#" + nextBelowId1).addClass("selected C32");
			exbSpace.push(arrId);
			
			spaceCount.c23++;
			$("#c23-value").html(spaceCount.c23);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}

function draw3x3(elm){
	
	var id = $(elm).attr("id");
	var nextId = getBlockOnRight(id);
	var nextId1 = getBlockOnRight(nextId);
	var belowId = getBlockOnBelow(id);
	var nextBelowId = getBlockOnBelow(nextId);
	var nextBelowId1 = getBlockOnBelow(nextId1);
	var belowBelowId = getBlockOnBelow(belowId);
	var nextBelowBelowId = getBlockOnBelow(nextBelowId);
	var nextBelowBelowId1 = getBlockOnBelow(nextBelowId1);
	
	var arrId = [id, nextId, nextId1, belowId, nextBelowId, nextBelowId1, belowBelowId, nextBelowBelowId, nextBelowBelowId1];
	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(id);
		
	}else{
		if(checkIfNoConflict(arrId)){
			$(elm).addClass("selected C33 C33-right C33-bottom").attr("data-stand-type", "C33");
			$("#" + nextId).addClass("selected C33 C33-right C33-bottom");
			$("#" + nextId1).addClass("selected C33 C33-bottom");
			$("#" + belowId).addClass("selected C33 C33-right C33-bottom");
			$("#" + nextBelowId).addClass("selected C33 C33-right C33-bottom");
			$("#" + nextBelowId1).addClass("selected C33 C33-bottom");
			$("#" + belowBelowId).addClass("selected C33 C33-right");
			$("#" + nextBelowBelowId).addClass("selected C33 C33-right");
			$("#" + nextBelowBelowId1).addClass("selected C33");
			
			exbSpace.push(arrId);
			
			spaceCount.c33++;
			$("#c33-value").html(spaceCount.c33);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}

function draw6x6(elm){
	
	var box = {};
	
	var arrId = [];
    for(var r = 1; r <= 6; r++){
    	for(var c = 1; c <= 6;c++){
    		if(r == 1 && c == 1){
    			box["r1c1"] =$(elm).attr("id");
    		}else if(c == 1){
    			box["r"+r+"c1"] =getBlockOnBelow(box["r"+(r-1)+"c1"]);
    		}else{
    			box["r"+r+"c"+c] =getBlockOnRight(box["r"+r+"c"+(c-1)]);
    		}
    		
    		arrId.push(box["r"+r+"c"+c]);
    	}
    	
    }

	
	
	
	
	if($(elm).hasClass("selected")){
		
		removeFromExb(box["r1c1"]);
		
	}else{
		if(checkIfNoConflict(arrId)){
			
			
		    for(var r = 1; r <= 6; r++){
		    	for(var c = 1; c <= 6;c++){
		    		
		    		$("#" + box["r"+r+"c"+c]).addClass("selected C66");
		    		
		    		if(r == 1 && c == 1){
		    			$("#" + box["r1c1"]).attr("data-stand-type", "C66").addClass("C66-right C66-bottom");
		    		}else if(r == 6 && c != 6){
		    			$("#" + box["r"+r+"c"+c]).addClass("C66-right");
		    		}else if(c == 1 && r != 6){
		    			$("#" + box["r"+r+"c"+c]).addClass("C66-right C66-bottom");
		    		}else if(r != 6 && c != 6){
		    			$("#" + box["r"+r+"c"+c]).addClass("C66-right C66-bottom");
		    		}else if(r != 6 && c == 6){
		    			$("#" + box["r"+r+"c"+c]).addClass("C66-bottom");
		    		}
		    		
		    		
		    	}
		    }
			

			
			
			exbSpace.push(arrId);
			
			spaceCount.c66++;
			$("#c66-value").html(spaceCount.c66);
			spaceCount.total++;
			$("#standTotal").html(spaceCount.total);
			
		}else{
			showHightlight(elm);
		}
	}
	generateStandNo();
	saveToFile();
}

function removeFromExb(id){
	
	var len = exbSpace.length; 
	
	for(var i = 0; i < len; i++){
		const index = exbSpace[i].indexOf(id);
		
		if(index !== -1){
			var slot = exbSpace[i];
			var type = "";
			for(var j = 0; j < slot.length; j++){
				var clazz1 = $("#" + slot[j]).attr("class");
				type = $("#" + slot[j]).attr("data-stand-type")
				upDateSummmary(type);
				$("#" + slot[j]).removeClass(clazz1).removeAttr("data-stand-type").removeAttr("data-stand-id").empty();
				$("#" + slot[j]).addClass(clazz);
				
				
			}
			
			exbSpace.splice(i, 1);
			return;
		}
	}
}

function getDataFromExb(id){
	
	var len = exbSpace.length; 
	
	for(var i = 0; i < len; i++){
		const index = exbSpace[i].indexOf(id);
		
		if(index !== -1){
			return exbSpace[i];
		}
	}
	return false;
}

function getBlockOnRight(id){
	
	
	var ids = id.split("_");
	
	var col = ids[1].replace( /^\D+/g, '');
	var row = ids[0].replace( /^\D+/g, '');
	
	
	
	var ncol = parseInt(col);
	
	return "R" + (row)+ "_C"+(ncol+1);
	
	
}
function getBlockOnBelow(id){

	var ids = id.split("_");
	
	var col = ids[1].replace( /^\D+/g, '');
	var row = ids[0].replace( /^\D+/g, '');
	
	
	
	var nrow = parseInt(row);
	
	return "R" + (nrow+1)+ "_C"+col;
}

function checkIfNoConflict(arrId){
	
	var len = exbSpace.length; 
	
	for(var i = 0; i < len; i++){
		
		if(hasCommonElement(exbSpace[i], arrId)){
			return false;
		}
	}
	
	for(i = 0; i < arrId.length; i++){
		var id = arrId[i];
		var ids = id.split("_");
		
		var col = ids[1].replace( /^\D+/g, '');
		var row = ids[0].replace( /^\D+/g, '');
		
		if(col > maxCol){
			return false;
		}
		if(row >= maxRow){
			return false;
		}
	}
	return true;
	
}
function hasCommonElement(arr1,arr2)
{
   var bExists = false;
   $.each(arr2, function(index, value){

     if($.inArray(value,arr1)!=-1){
        console.log(value);
        bExists = true;
     }

     if(bExists){
         return false;  //break
     }
   });
   return bExists;
}

function showHightlight(elm){
	
	    $(elm).addClass("redbg");
	    setTimeout(function(){
	    	
	    	$(elm).removeClass("redbg");
	    	
	    }, 500);
	
	
}
function generateStandNo(){
	
	var row = 1;
	var col = 1;
	var count = 1;
	while(row < maxRow){
		col = 0;
		while(col < maxCol+1){
		
			if(checkIfFirstSelectedElement("R"+row+"_C"+col) && ($("#R"+row+"_C"+col).hasClass("selected") || $("#R"+row+"_C"+col).hasClass("selectedPublic")) ){
				
				var type = $("#R"+row+"_C"+col).attr('data-stand-type');
				
				if(type != "C11"){
				
					$("#R"+row+"_C"+col).html("<span style='position: absolute;' class='font-"+type+"'>" + count + "</span>").attr("data-stand-id", "S"+count);
				
				}
				var text = getTextBySize(type);	
				$("#R"+row+"_C"+col).attr({"data-toggle":"tooltip", "data-html":"true", "title":count + ".<br> " + text, "data-stand-id": "S"+count});
				count++;
				
			}
			col++;
		}
		row++;
	}

	
}
function checkIfFirstSelectedElement(elm){
	
	var len = exbSpace.length; 
	
	for(var i = 0; i < len; i++){
		var index = exbSpace[i][0];
		
		if(index == elm){
			return true;
		}
	}
	return false;
}

function saveToFile(){
	
	
	var data = $("div[data-stand-type]");
	//var structure = $("div[data-structure]");
	
	var dataOutput = {
			height: inputHeight,
			width: inputWidth,
			stands: []
			//structure: []
			
	};
	
	$.each(data, function(i, elm){
		
		var stand = {
				standId: $(elm).attr("data-stand-id"),
				standType: $(elm).attr("data-stand-type"),
				standLocation: $(elm).attr("id"),
		}
		
		dataOutput.stands.push(stand);
	});
	/*
	$.each(structure, function(i, elm){
		
		var stand = {
				standType: $(elm).attr("data-stand-type"),
				standLocation: $(elm).attr("id"),
		}
		
		dataOutput.structure.push(stand);
	});
	*/
	//$("#saveJsonArea").show();
	$("#stand_map_json").val(JSON.stringify(dataOutput));
	
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function drawIntoGrid(i, elm){
	
	var loc = "#" + elm.standLocation;
	
	if(editMode || liveView){
		
		switch(elm.standType){
			case "C11": draw1x1(loc); break;
			case "C12": draw1x2(loc); break;
			case "C21": draw2x1(loc); break;
			case "C22": draw2x2(loc); break;
			case "C23": draw2x3(loc); break;
			case "C32": draw3x2(loc); break;
			case "C33": draw3x3(loc); break;
			case "C66": draw6x6(loc); break;
			case "walkway": drawWalkway(loc); break;
			case "wallV": drawWall(loc, "V"); break;
			case "wallH": drawWall(loc, "H"); break;
			case "wallD"	: drawWall(loc, "D"); break;
			case "wallB": drawWall(loc, "B"); break;
			case "wallT": drawWall(loc, "T"); break;
			case "wallTC" : drawWall(loc, "TC"); break;
			case "wallP" : drawWall(loc, "P"); break;
			case "wallEP": drawWall(loc, "EP"); break;
			case "wallE3P" : drawWall(loc, "E3P"); break;
			case "wallPH" : drawWall(loc, "PH"); break;
			case "wallWH": drawWall(loc, "WH"); break;
			case "wallWV": drawWall(loc, "WV"); break;
		}
	}
	return;
	
	
}

function checkStandIfAvailable(elm){
	
	$("#stand_no_error").empty();
	
	var stand_no = $(elm).val();
	var eventId = $("#stand_booking_event_id_select").val(); 
	var bookingId = $("#stand_booking_post_id").val(); 
	
    jQuery.ajax({      
        type:    "POST",
        dataType: "json",
        url:     MBAjax.ajaxurl,
        data:    {
      	  "action":'check_stand_availablity',
      	  "stand_no":stand_no,
      	  "event_id": eventId,
      	  "booking_id": bookingId
        },
        success: function(data) {
           if(data.status == "PENDING" || data.status == "CONFIRMED"){
        	   $("#stand_no_error").html(data.message);
           }
        }
      });
}

function floorplanfirstStageNext(){
	$("#floorplan_step").html("Step 2");
	
	$("#error").html("");
	var height = $("#height_input").val();
	var width = $("#width_input").val();
	
	if(height > 100 || width > 100 || (height > 100 && width > 100)){
		$("#error").html("Error: Height and Width must be greater than 10x10 and less than 100x100");
		return;
	}
	
	if(height < 10 || width < 10 || (height < 10 && width < 10)){
		$("#error").html("Error: Height and Width must be greater than 10x10 and less than 100x100");
		return;
	}
	$("#first_stage").hide();
	$("#second_stage").show();
	editMode = true;
	
	if(!$("#btnWalkway").hasClass("spaceSizeSelected")){
		$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
		spaceSelect = "Walkway";
		$("#btnWalkway").addClass("spaceSizeSelected");
		walkwaySelected = true;
	}
	
	createGrid(height, width, "#gridArea");
	
	$("#floorplan_back").show();
	
	fillPreExisting();
	
	floorplanStage++;
	
}

function floorplanfirstStageBack(){
	
	
	
}

function floorplanSecondStageNext(){
	
	$("#floorplan_step").html("Step 3");
	$("#first_stage").hide();
	$("#second_stage").hide();
	$("#third_stage").show();
	
	if(!$("#btns11").hasClass("spaceSizeSelected")){
		$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
		spaceSelect = "s11";
		$("#btns11").addClass("spaceSizeSelected");
		walkwaySelected = false;
	}
	
	floorplanStage++
}
function floorplanSecondStageBack(){
	
	
	$("#floorplan_step").html("Step 1");
	$("#error").html("");
	$("#height_input").val(inputHeight);
	$("#width_input").val(inputWidth);
	$("#first_stage").show();
	$("#second_stage").hide();
	
	$("#floorplan_back").hide();
	
	editMode = true;
	floorplanStage--;
}

function floorplanThirdStageNext(){
	if(editMode){
		$("#floorplan_step").html("Step 4 - Preview");
		generateStandNo();
		editMode = false;
		$("#first_stage").hide();
		$("#second_stage").hide();
		$("#third_stage").hide();
		
		$("#gridbox").removeClass("grid").addClass("grid-admin-output");
		$(".walkway").addClass("walkway-output");
		$("#floorplan_next").hide();
		floorplanStage++;
	}
}
function floorplanThirdStageBack(){
	
	$("#floorplan_step").html("Step 2");
	$("#second_stage").show();
	$("#third_stage").hide();
	
	if(!$("#btnWalkway").hasClass("spaceSizeSelected")){
		$("#btn"+spaceSelect).removeClass("spaceSizeSelected");
		spaceSelect = "Walkway";
		$("#btnWalkway").addClass("spaceSizeSelected");
		walkwaySelected = true;
	}
	
	floorplanStage--;
	
}

function floorplanFourthStageBack(){
	
	$("#floorplan_next").show();
	$("#floorplan_step").html("Step 2");
	$("#first_stage").hide();
	$("#second_stage").show();
	$("#third_stage").hide();
	
	$("#gridbox").addClass("grid").removeClass("grid-admin-output");
	$(".walkway").removeClass("walkway-output");
	
	floorplanStage--;
	
	editMode = true;
	
}


function fillPreExisting(){
	
	
var preexisting = $("#stand_map_json").val();
	
	if(IsJsonString(preexisting)){
		
		var json = JSON.parse(preexisting);
		
		exbSpace = [];
		
		editMode = true;
		
		$.each(json.stands, drawIntoGrid);
		$.each(json.structure, drawIntoGrid);
		
		
		
	}
	
	
}
function upDateSummmary(type){
	
	switch(type){
		case "C11":
			spaceCount.c11--;
			$("#c11-value").html(spaceCount.c11);
			break;
		case "C12":
			spaceCount.c21--;
			$("#c21-value").html(spaceCount.c21);
			break;
		case "C21":
			spaceCount.c21--;
			$("#c21-value").html(spaceCount.c21);
			break;
		case "C22":
			spaceCount.c22--;
			$("#c22-value").html(spaceCount.c22);
			break;
		case "C23":
			spaceCount.c32--;
			$("#c23-value").html(spaceCount.c23);
			break;
		case "C32":
			spaceCount.c23--;
			$("#c23-value").html(spaceCount.c23);
			break;
		case "C33":
			spaceCount.c33--;
			$("#c33-value").html(spaceCount.c33);
			break;
		case "C66":
			spaceCount.c66--;
			$("#c66-value").html(spaceCount.c66);
			break;
		default:
			return;
	}
	
	spaceCount.total--;
	$("#standTotal").html(spaceCount.total);
	
}
function getTextBySize(type){
	switch(type){
	case "C11":
		return "1M X 1M";
	case "C12":
		return "1M X 2M";
	case "C21":
		return "2M X 1M";
	case "C22":
		return "2M X 2M";
	case "C23":
		return "2M X 3M";
	case "C32":
		return "3M X 2M";
	case "C33":
		return "3M X 3M";
	case "C66":
		return "6M X 6M";
	default:
		return;
	}
}
var selectedType = "";
var rotationClass = "";
function addRotatePreview(type, rot){
	
	$('#imgPreview').empty();
	selectedType = type;
	var path = $("#img_path").val();
	switch(type){
	
		case "SD":
			path += "single-door.png";
			break;
		case "DD":
			path += "double-door.png";
			break;
		default:
			return;
	
	}
	
	switch (rot) {
		case 0:
			rotationClass = "";
			break;
		case 90:
			rotationClass = "rot90";
			break;
		case 180: 
			rotationClass = "rot180";
			break;
		case 270:
			rotationClass = "rot270";
			break;
		default:
			return;
	}
	
	
	
	$('#imgPreview').prepend('<img src="'+path+'" class="'+rotationClass+'" />');
	
}
