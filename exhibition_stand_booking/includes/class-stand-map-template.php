<?php
function stand_booking_event_id_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );
	
	//$value = get_post_meta( $post->ID, '_stand_booking_event_id', true );
	
	echo '<input type="hidden" id="stand_booking_post_id" name="stand_booking_post_id" value="' . esc_attr( $post->ID ) . '"/>';
	

	$value = get_post_meta( $post->ID, '_event_stand_map_id', true );
	
	$query = new WP_Query( array( 'post_type' => 'events' ) );
	
	if ( $query->have_posts() ) : ?>
		
		<select id="stand_booking_event_id_select" name="stand_booking_event_id">
		
	    <?php while ( $query->have_posts() ) :
	    	$query->the_post(); 
	    	$selected = "";
	    	if(strcmp(the_title(), $value) == 0){
	    		$selected = "selected='selected'";
	    	}
	    	?>   
	        <option value="<?php echo get_the_ID();?>" <?php echo $selected?>>
	            <?php the_title(); ?>
	        </option>
	    <?php endwhile; wp_reset_postdata(); ?>
	    </select>
		<?php endif; 


}
function stand_booking_company_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );
	
	$value = get_post_meta( $post->ID, '_stand_booking_company', true );
	
	echo '<input style="width:100%" id="stand_booking_company" name="stand_booking_company" value="' . esc_attr( $value ) . '"/>';
}
function stand_booking_name_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );
	
	$value = get_post_meta( $post->ID, '_stand_booking_name', true );
	
	echo '<input style="width:100%" id="stand_booking_name" name="stand_booking_name" value="' . esc_attr( $value ) . '"/>';
}
function stand_booking_phone_number_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );
	
	$value = get_post_meta( $post->ID, '_stand_booking_phone', true );
	
	echo '<input type="number" style="width:100%" id="stand_booking_phone_number" name="stand_booking_phone_number" value="' . esc_attr( $value ) . '"/>';
}
function stand_booking_email_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );
	
	$value = get_post_meta( $post->ID, '_stand_booking_email', true );
	
	echo '<input type="email" style="width:100%" id="stand_booking_email" name="stand_booking_email" value="' . esc_attr( $value ) . '"/>';
}
function stand_booking_stand_no_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );
	
	$value = get_post_meta( $post->ID, '_stand_booking_stand_no', true );
	
	echo '<input style="width:100%" id="stand_booking_stand_no_input" name="stand_booking_stand_no" value="' . esc_attr( $value ) . '"/><br/><p id="stand_no_error"></p>';
}

function stand_booking_status_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );

	$value = get_post_meta( $post->ID, '_stand_booking_status', true );

	$arr = array("CONFIRMED", "PENDING", "AVAILABLE");

	 ?>
	
	<select id="stand_booking_status" name="stand_booking_status">
	
    <?php foreach ($arr as $a):
    	$selected = "";
    
    	if(strcmp($a, $value) == 0){
    		$selected = "selected='selected'";
    	}
    	?>   
        <option value="<?php echo $a;?>" <?php echo $selected?>>
            <?php echo $a; ?>
        </option>
    <?php endforeach; ?>
    </select>
	<?php 
	
	//echo '<input style="width:100%" id="event_stand_map_id" name="event_stand_map_id" value="' . esc_attr( $value ) . '"/>';
}

function stand_booking_stand_size_meta_box_callback($post){
	
	global $stand_space_array;
	
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_booking_nonce', 'stand_booking_nonce' );
	
	$value = get_post_meta( $post->ID, '_stand_booking_stand_size', true );
	
	$arr = $stand_space_array; //array("C11", "C12", "C21", "C22", "C32");
	
	?>
	
	<select id="stand_booking_stand_size" name="stand_booking_stand_size">
	
    <?php foreach ($arr as $a):
    	$selected = "";
    
    	if(strcmp($a["type"], $value) == 0){
    		$selected = "selected='selected'";
    	}
    	?>   
        <option value="<?php echo $a["type"];?>" <?php echo $selected?>>
            <?php echo $a["text"]; ?>
        </option>
    <?php endforeach; ?>
    </select>
	<?php 
	
	//echo '<input style="width:100%" id="event_stand_map_id" name="event_stand_map_id" value="' . esc_attr( $value ) . '"/>';
}

function event_stand_map_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'events_nonce', 'events_nonce' );
	
	$value = get_post_meta( $post->ID, '_event_stand_map_id', true );

	$query = new WP_Query( array( 'post_type' => 'stand_booking_map' ) );
	
	if ( $query->have_posts() ) : ?>
	
	<select id="event_stand_map_id" name="event_stand_map_id">
	
    <?php while ( $query->have_posts() ) : 
    
    	$query->the_post(); 
    	$selected = "";
    	
    	if(strcmp( (string) get_the_ID(), $value) == 0){
    		
    		$selected = "selected='selected'";
    	}
    	?>   
        <option value="<?php echo get_the_ID();?>" <?php echo $selected?>>
            <?php the_title(); ?>
        </option>
    <?php endwhile; wp_reset_postdata(); ?>
    </select>
    <br/>
    <br/>
	 
	<p>OR</p>
	
    
	<?php endif;?>
	
	<p>Create a New Floor Plan by clicking below </p>
	<br/>
	
	<a href="<?php echo admin_url('post-new.php?post_type=stand_booking_map');?>" class="page-title-action">Create New Floor Plan</a>
	
	<?php 
	//echo '<input style="width:100%" id="event_stand_map_id" name="event_stand_map_id" value="' . esc_attr( $value ) . '"/>';
}
function event_date_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'events_nonce', 'events_nonce' );
	
	$value = get_post_meta( $post->ID, '_event_date', true );
	
	echo '<input type="date" style="width:100%" id="event_date" name="event_date" value="' . esc_attr( $value ) . '"/>';
}


function stand_map_meta_box_callback($post){
	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'stand_map_nonce', 'stand_map_nonce' );
	
	$value = get_post_meta( $post->ID, '_stand_map', true );
	
	//echo '<textarea style="width:100%" id="global_notice" name="global_notice">' . esc_attr( $value ) . '</textarea>';
	
	?>
	
<div id="stand_map_area" class="container">
<section class="content">

<h1>Floor Plans Widget</h1>

	<div style="width:100%">

		<div id="gridArea">
			
		</div>
		
		<div id="floorplanSummary">
		<table class="table-summary">
			<tr><th>Stand Size</th><th>No of Stand</th></tr>
			<tr><td align="right"><span class="c11-legend">&#9632 </span> 1m X 1m: </td><td>  <span id="c11-value">0</span></td></tr>  
			<tr><td align="right"><span class="c12-legend">&#9632 </span><span class="c21-legend">&#9632 </span> 1m X 2m: </td><td> <span id="c21-value">0</span></td></tr> 
			<tr><td align="right"><span class="c22-legend">&#9632 </span> 2m X 2m: </td><td>  <span id="c22-value">0</span></td></tr>
			<tr><td align="right"><span class="c23-legend">&#9632 </span><span class="c32-legend">&#9632 </span> 3m X 2m: </td><td>  <span id="c23-value">0</span></td></tr>
			<tr><td align="right"><span class="c33-legend">&#9632 </span> 3m X 3m: </td><td>  <span id="c33-value">0</span></td></tr>
			<tr><td align="right"><span class="c66-legend">&#9632 </span> 6m X 6m: </td><td>  <span id="c66-value">0</span></td></tr>
			<tr><td>Total:</td><td><span id="standTotal">0</span></td></tr>
		</table>
		</div>
	
	</div>
	<div style="width:100%" class="clear">
	<h1 id="floorplan_step" style="clear: both;">Step 1</h1>
	<div id="first_stage">
	
		<h2 id="floorplan_instruction">Select Hall Mearsurement in Metres</h2>

				<table>
				
					<tr> 
						<td><label for="height_input">Height:</label></td>
						<td><input class="input" type="number" id="height_input" name="height_input" placeholder="Enter Height" /></td>
					
					</tr>

					<tr>
					
						<td><label for="width_input">Width:</label></td>
						<td><input class="input" type="number" id="width_input" name="width_input"  placeholder="Enter Width"/></td>
						
					</tr>
				
				</table>
				<br/>
				<p id="error"></p>
				
	</div>
	<div id="second_stage">
		<h2>Outline your room features.</h2>
		<div id="structure_area" class="spaceSelection">
			<div id="btnWalkway" class="btn">Walkway</div>
			<div id="btnWallH" class="btn">Horizontal Wall</div>
			<div id="btnWallV" class="btn">Vertical Wall</div>
			<div id="btnStructureBlock" class="btn">Wall Junction</div>
			<div id="btnPillarBlock" class="btn">Pillar</div>
			<div id="btnDoor" class="btn" data-set-rotation="true">Door</div>
			<div id="btnDoubleDoor" class="btn" data-set-rotation="true">Double Door</div>
			<div id="btnToliet" class="btn">Toilet</div>
			<div id="btnTable" class="btn">Table and Chair</div>
			<div id="btnWindowH" class="btn">Horizontal Window</div>
			<div id="btnWindowV" class="btn">Vertical Window</div>
			<div id="btnPower" class="btn">Electric Point</div>
			<div id="btnThreePhase" class="btn">Three Phase Electric Point</div>
			<div id="btnPhone" class="btn">Telephone Point</div>
		</div>
		
		<div id="rotate" style="display: none;">
		
			<h2>Rotate?</h2>
			<div id="imgPreview"></div>
			<div class="rotationSelection">
				<div id="btnRotateLeft" class="btn">Rotate Left</div>
				<div id="btnRotateRight" class="btn">Rotate Right</div>	
			</div>
		</div>
		
	</div>
	
	<div id="third_stage">
		<h2>Outline your Exbition Stands.</h2>
		<div id="space_area" class="spaceSelection">
			<div id="btns11" class="C11 btn">1x1 Space</div>
			<div id="btns12" class="C12 btn">1x2 Space Horizontal</div>
			<div id="btns21" class="C21 btn">1x2 Space Vertical</div>
			<div id="btns22" class="C22 btn">2x2 Space</div>
			<div id="btns32" class="C32 btn">3x2 Space Horizontal</div>
			<div id="btns23" class="C23 btn">3x2 Space Vertical</div>
			<div id="btns33" class="C33 btn">3x3 Space</div>
			<div id="btns66" class="C66 btn">6x6 Space</div>
		</div>
	
	</div>	

		<br/>
		<div class="spaceSelection">
			<div id="floorplan_back" class="btn">Back..</div>
			<div id="floorplan_next" class="btn">Next..</div>
			
		</div>
		
		
		<div id="saveJsonArea" style="display:none;">
			<input type="text" id="stand_map_json" name="stand_map_json" value="<?php echo esc_attr( $value ); ?>"/>
			<input type="text" id="img_path" name="img_path" value="<?php echo   plugins_url( '../public/img/', __FILE__ ); ?>"/>
		</div>
		</div> 
</section>
</div>
	
	<?php

}