<?php 

function events_template($single) {
	
	global $wp_query, $post;
	
	/* Checks for single template by post type */
	if ( $post->post_type == 'events' ) {
		
		
		if ( file_exists( plugin_dir_path( __FILE__ ) . 'template/template_events.php' ) ) {
			return plugin_dir_path( __FILE__ ) . 'template/template_events.php';
		}
		
		
	}
	
	return $single;
	
}

function  getBookedStand($eventId) {
	
	
	$loop = new WP_Query( array(
			'post_type' => 'stand_booking',
			'posts_per_page' => -1,
			'meta_query' => array(
					array('key' => '_stand_booking_event_id', 'value' => $eventId)
			)
	) );
	
	// The Loop
	
	$standId = array();
	
	if ( $loop->have_posts() ) {
		
		while ( $loop->have_posts() ) {
			$loop->the_post();
			
			$arr = array();
			$arr["title"] = get_the_title();
			$arr["standId"] = get_post_meta( get_the_ID() , '_stand_booking_stand_no' , true );
			$standId[] = $arr;
		}
		
		/* Restore original Post Data */
		wp_reset_postdata();
	}
	
	echo json_encode($standId);
	
}

function  getBookedStandName($eventId) {
	
	
	$loop = new WP_Query( array(
			'post_type' => 'stand_booking',
			'posts_per_page' => -1,
			'meta_query' => array(
					array('key' => '_stand_booking_event_id', 'value' => $eventId),
					array('key' => '_stand_booking_status', 'value' => "CONFIRMED")
			)
	) );
	
	// The Loop
	if ( $loop->have_posts() ) {
		echo '<table class="confirmed-table">'.
		"<tr><th>Stand No.</th><th>Company</th></tr>";
		while ( $loop->have_posts() ) {
			$loop->the_post();
			echo '<tr><td>'.get_post_meta( get_the_ID() , '_stand_booking_stand_no' , true ).'</td><td>' . get_the_title() . '</td></tr>';
		}
		echo '</table>';
		/* Restore original Post Data */
		wp_reset_postdata();
	} else {
		// no posts found
		
		echo "No Confirmed Booking Yet";
	}
	
}

?>