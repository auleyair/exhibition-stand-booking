<?php 

/**
 * Template Name: Full Width Page
 *
 * @since             1.0.0
 * @package           Exhibition_stand_booking
 * Author:            Pauraic Mc Auley
 *
 */

global $wp_query, $post;

?>
<?php $mapId = get_post_meta($post->ID, "_event_stand_map_id", true);?>
<?php get_header(); ?>

<div class="container">
	
	<div>
	<h1><?php echo get_the_title();?></h1>
	<p><?php echo date("l jS \of F Y", strtotime(get_post_meta($post->ID, "_event_date", true)));?></p>
		<script type="text/javascript">


		bookedStand = <?php getBookedStand($post->ID);?>;
		
		</script>
		
		<h1>Floor Plan</h1>
		
		<!-- p style="text-align:center;"><span class="c11-legend legend">&#9632 </span> 1m X 1m  <span class="c12-legend legend">&#9632 </span> 1m X 2m <span class="c21-legend legend">&#9632 </span> 2m X 1m<br/>
		<span class="c22-legend legend">&#9632 </span> 2m X 2m  <span class="c32-legend legend">&#9632 </span> 3m X 2m <span class="booked legend">&#9632</span>Space Booked
		</p-->			
		<div id="frontend_stand_map"></div>
		<?php wp_nonce_field( 'make_stand_booking_nonce', 'make_stand_booking_nonce' );?>
		<input type="hidden" id="frontend_stand_map_json" name="frontend_stand_map_json" value="<?php echo esc_attr(  get_post_meta($mapId, "_stand_map", true) ); ?>"/>
		<input type="hidden" id="img_path" name="img_path" value="<?php echo   plugins_url( '../../public/img/', __FILE__ ); ?>"/>
	</div>
	
	<div>
		<h3>Form Area</h3>
		
		<form class="form" id="ajax-booking-form">
			<input type="hidden" name="event_id" id="event_id"  value="<?php echo $post->ID;?>" readonly>
			<input type="text" name="stand_no" id="stand_no"  placeholder="Stand No" readonly>
			<input type="hidden" name="stand_size" id="stand_size" readonly>
			<input type="text" name="company" id="company"  placeholder="Company Name" required>
			<input type="text" name="name" id="name"  placeholder="Full Name" required>
			<input type="email" name="email" id="email"  placeholder="Email" required>                            
        	<input type="number" name="phone" id="phone"  placeholder="Phone" required>
        	<input type="submit" class="btn">
		</form>
		
		<div id="feedback"></div>
		
		<div>
			<h2>Confirmed Booking</h2>
			<?php getBookedStandName($post->ID);?>
		</div>
	</div>
	
</div>

<?php get_footer(); ?>
