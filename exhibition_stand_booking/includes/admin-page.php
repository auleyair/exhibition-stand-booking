<?php 

// Add the custom columns to the book post type:

function set_custom_edit_stand_booking_columns($columns) {
	$columns['event_id'] = __( 'Event', 'stand_booking' );
	$columns['stand_no'] = __( 'Stand No', 'stand_booking' );
	$columns['stand_size'] = __( 'Stand Size', 'stand_booking' );
	$columns['status'] = __( 'Status', 'status' );
	
	return $columns;
}
//stand_booking_stand_size
// Add the data to the custom columns for the book post type:
function custom_stand_booking_column( $column, $post_id ) {
	switch ( $column ) {
		
		case 'event_id' :
			$terms = get_the_title(get_post_meta( $post_id , '_stand_booking_event_id' , true ));
			if ( is_string( $terms ) )
				echo $terms;
				else
					_e( 'Unable to get event', 'stand_booking' );
					break;
					
		case 'stand_no' :
			echo get_post_meta( $post_id , '_stand_booking_stand_no' , true );
			break;
			
		case 'status' :
			echo get_post_meta( $post_id , '_stand_booking_status' , true );
			break;
		case 'stand_size':
			echo get_space_size_text(get_post_meta( $post_id , '_stand_booking_stand_size' , true ));
			break;
			
	}
}

function get_space_size_text($type){
	global $stand_space_array;
	
	foreach ($stand_space_array as $a){
		
		if(strcmp($type, $a["type"]) == 0){
			return $a["text"];
		}
		
	}
	
	return "N/A";
	
	
}

?>